# Taskmanager



## Projeto

Projeto desenvolvido no módulo Aprendendo na prática da Formação Python da Rocketseat.
O projeto consiste em um gerenciador de tarefas que roda no terminal.

## Funcionalidades
- [X] Adicionar tarefa.
- [X] Listar tarefas.
- [X] Editar nome da tarefa.
- [X] Marcar tarefa com concluída.
- [X] Remover tarefas concluídas da lista de tarefas.


