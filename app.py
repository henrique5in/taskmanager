def adicionar_tarefa(tasks_list, task_name):
  task = {"task_name": task_name, "completed": False}
  tasks_list.append(task)
  print(f"A tarefa {task_name} foi adicionada com sucesso!")
  return

def listar_tarefas(tasks_list):
  print("\nListando tarefas...")
  if len(tasks_list) == 0:
    print("Nenhuma tarefa cadastrada!")
    return
  for indice, task in enumerate(tasks_list):
    print(f"{indice + 1} - [{'Concluída' if task['completed'] else 'Pendente'}] - {task['task_name']}")
  return
  

def atualizar_tarefa(taks_list, task_index, new_task_name):
  new_index = task_index - 1
  if new_index >= 0 and new_index < len(taks_list):
    tasks_list[new_index]["task_name"] = new_task_name
    print(f"Tarefa {task_index} atualizada para {new_task_name} com sucesso!")
    return
  else:
    print("Tarefa não encontrada!")
    return
    
def completar_tarefa(taks_list, task_index):
  new_index = task_index - 1
  if new_index >= 0 and new_index < len(taks_list):
    tasks_list[new_index]["completed"] = True
    print(f"Tarefa {task_index} concluída com sucesso!")
    return
  else:
    print("Tarefa não encontrada!")
    return

def remover_tarefa_concluida(tasks_list):
  for task in tasks_list:
    if task["completed"]:
      tasks_list.remove(task)
  print("Tarefas completadas removidas com sucesso!")
  return

tasks_list = []
while True:
  print("\nGerenciador de Tarefas:")
  print("1 - Adicionar Tarefa")
  print("2 - Listar Tarefas")
  print("3 - Atualizar Tarefa")
  print("4 - Completar Tarefa")
  print("5 - Remover Tarefa Concluída")
  print("6 - Sair")

  opcao = input("\nDigite a opção desejada: ")

  if opcao == "1":
    task = input("Digite a tarefa que deseja adicionar: ")
    adicionar_tarefa(tasks_list, task)
  elif opcao == "2":
    listar_tarefas(tasks_list)
  elif opcao == "3":
    listar_tarefas(tasks_list)
    task_index = int(input("Digite o número da tarefa que deseja atualizar: "))
    new_task_name = input("Digite o novo nome da tarefa: ")
    atualizar_tarefa(tasks_list, task_index, new_task_name)
  elif opcao == "4":
    listar_tarefas(tasks_list)
    task_index = int(input("Digite o número da tarefa que deseja marcar como concluída: "))
    completar_tarefa(tasks_list, task_index)
  elif opcao == "5":
    remover_tarefa_concluida(tasks_list)
    listar_tarefas(tasks_list)
  elif opcao == "6":
    print("Programa finalizado com sucesso!\n")
    break
  else:
    print("Opção inválida!")